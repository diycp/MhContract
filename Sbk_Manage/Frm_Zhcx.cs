﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Sbk_Manage
{
    public partial class Frm_Zhcx : Form
    {
        public Frm_Zhcx()
        {
            InitializeComponent();
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            this.TxtHtBh.Text = "";
            this.TxtSbMc.Text = "";
            this.TxtHtMc.Text = "";
            this.Txtghs.Text = "";
            this.TxtSbBarcode.Text = "";
            this.TxtSbBfpzwh.Text = "";
            this.TxtSbCj.Text = "";
            this.TxtSbsyks.Text = "";
            this.dtpSbysrq.Checked = false;
            this.TxtHtlrnf.Text="";
            ToolStripStatusLabel1.Text = "";
        }
        private void DivPageSelect(Boolean FX)
        {
            //判断设置按钮可用性 下一页
            if (FX == true)
            {
                CurPage += 1;
                if (CurPage == TotalPages)
                {
                    //已经到了最后一页，置'下一页'按钮不再可用
                    BtnNext.Enabled = false;
                    BtnPreview.Enabled = true;
                }
                else
                {
                    BtnPreview.Enabled = true;
                }
            }
            //上一页
            if (FX == false)
            {
                CurPage -= 1;
                if (CurPage == 1)
                {
                    //已经到了第一页，置'上一页'按钮不再可用
                    BtnPreview.Enabled = false;
                    BtnNext.Enabled = true;
                }
                else
                {
                    BtnNext.Enabled = true;
                }
            }
            //形成查询字符串
            string sqlstr = SqlPageStr + SqlQueryStr.ToString() + " order by b.id desc LIMIT  " + offsetRows + " OFFSET  " + (CurPage - 1) * offsetRows;
            DgvSbInfo.DataSource = Program.SqliteDB.GetDataTable(sqlstr);
            if (DgvSbInfo.Rows.Count > 0)
            {
                ToolStripStatusLabel1.Text = "共" + TotalPages + "页；当前第" + CurPage + "页；显示" + DgvSbInfo.Rows.Count.ToString() + "条信息";
            }
        }
        //初次查询
        private void InitLoad()
        {
            //分页查询，第一步是查询出总记录数；之后再分页
            string strSqlCount = "select count(*) from ht_info a left join sb_info b on a.htbh=b.htbh where a.del_flag=0  " + SqlQueryStr.ToString();
            //总行数
            TotalRows = Convert.ToInt32(Program.SqliteDB.ExecuteScalar(strSqlCount));

            if (TotalRows > 0)
            {
                //总页数
                LastPageRows = TotalRows % offsetRows;
                if (LastPageRows == 0)
                {
                    TotalPages = TotalRows / offsetRows;
                }
                else
                {
                    TotalPages = TotalRows / offsetRows + 1;
                }
                //设置‘上一页’和‘下一页’按钮的可用性
                if (TotalPages > 1)
                {
                    BtnNext.Enabled = true;
                }
                else
                {
                    BtnNext.Enabled = false;
                }
                //第一次上一页不可用
                BtnPreview.Enabled = false;
                //当前页
                CurPage = 1;
                //显示限定第一页内容；形成查询字符串
                string sqlstr = SqlPageStr + SqlQueryStr.ToString() + " order by b.id desc LIMIT  " + offsetRows + " OFFSET  " + (CurPage - 1) * offsetRows;
                DgvSbInfo.DataSource = Program.SqliteDB.GetDataTable(sqlstr);
                if (DgvSbInfo.Rows.Count > 0)
                {
                    ToolStripStatusLabel1.Text = "共" + TotalPages + "页；当前第" + CurPage + "页；显示" + DgvSbInfo.Rows.Count.ToString() + "条信息";
                }
            }
            else
            {
                BtnPreview.Enabled = false;
                BtnNext.Enabled = false;
                //清空原有数据
                if (DgvSbInfo.Rows.Count > 0)
                {
                    for (int i = DgvSbInfo.Rows.Count - 1; i >= 0; i--)
                    {
                        DgvSbInfo.Rows.RemoveAt(i);
                    }
                }
                ToolStripStatusLabel1.Text = "不存在满足此条件的信息！请重置查询条件后查询！";
            }
        }
        //默认每页显示记录数
        int offsetRows = 120;
        //当前页数
        int CurPage = 1;
        //总记录数
        int TotalRows = 0;
        //总页数
        int TotalPages = 0;
        //最后一页的记录数
        int LastPageRows = 0;
        string SqlPageStr = "select a.id as htid,b.id as id,a.htbh as htbh,xmbh,zbdlgs,htmc,hte,mc,xhgg,sbsn,barcode,bp,zczh,syks,ghsmc,ghskpxx,ghslxfs,cjmc,cjlxfs,zbq,sbsl,danjia,memo_note,sblb,tf_flag,jk_flag,wmgsmc,bgd,jyjyzm,zybkzpjb,bf_flag,bfwjph,strftime('%Y-%m-%d',htqdrq) as htqdrq,strftime('%Y-%m-%d',htysrq) as htysrq,strftime('%Y-%m-%d %H:%M:%S',htlrrq) as htlrrq,htlrr,scan_flag,jc_flag,strftime('%Y-%m-%d',ysrq) as ysrq,strftime('%Y-%m-%d',rzrq) as rzrq,strftime('%Y-%m-%d',bfrq) as bfrq,strftime('%Y-%m-%d',xcjyrq) as xcjyrq from ht_info a left join sb_info b on a.htbh=b.htbh where a.del_flag=0  ";
        //定义查询字符串
        StringBuilder SqlQueryStr = new StringBuilder();
        private void BtnQuery_Click(object sender, EventArgs e)
        {
            BtnQuery.Focus();
            //每次查询都清空
            SqlQueryStr.Remove(0, SqlQueryStr.Length);
            //组合条件
            if (!TxtHtBh.Text.Trim().Equals(""))
            {
                SqlQueryStr.AppendFormat(" and a.htbh='{0}'", TxtHtBh.Text.Trim().Replace("'", ""));
            }
            if (!Txtghs.Text.Trim().Equals(""))
            {
                SqlQueryStr.AppendFormat(" and ghsmc like '%{0}%'", Txtghs.Text.Trim().Replace("'", ""));
            }
            if (!this.TxtSbMc.Text.Trim().Equals(""))
            {
                SqlQueryStr.AppendFormat(" and mc like '%{0}%'", TxtSbMc.Text.Trim().Replace("'", ""));
            }
            if (!this.TxtHtMc.Text.Trim().Equals(""))
            {
                SqlQueryStr.AppendFormat(" and htmc like '%{0}%'", this.TxtHtMc.Text.Trim().Replace("'", ""));
            }
            if (!this.TxtSbBarcode.Text.Trim().Equals(""))
            {
                SqlQueryStr.AppendFormat(" and barcode = {0}", this.TxtSbBarcode.Text.Trim().Replace("'", ""));
            }
            if (!this.TxtSbsyks.Text.Trim().Equals(""))
            {
                SqlQueryStr.AppendFormat(" and  syks like '%{0}%'", TxtSbsyks.Text.Trim().Replace("'", ""));
            }
            if (!TxtSbCj.Text.Trim().Equals(""))
            {
                SqlQueryStr.AppendFormat(" and cjmc like '%{0}%'", TxtSbCj.Text.Trim().Replace("'", ""));
            }
            if (!TxtSbBfpzwh.Text.Trim().Equals(""))
            {
                SqlQueryStr.AppendFormat(" and bfwjph = {0}", TxtSbBfpzwh.Text.Trim().Replace("'", ""));
            }
            if (this.ChkSbysrq.Checked)
            {
                SqlQueryStr.AppendFormat(" and ysrq >= '{0}'", this.dtpSbysrq.Value.Date.ToString("s"));
            }
            if (!TxtHtlrnf.Text.Trim().Equals(""))
            {
                SqlQueryStr.AppendFormat(" and  strftime('%Y', htlrrq) = '{0}'", TxtHtlrnf.Text.Trim().Replace("'", ""));
            }
            //初次查询
            InitLoad();
        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            DivPageSelect(false);
        }

        private void BtnNext_Click(object sender, EventArgs e)
        {
            DivPageSelect(true);
        }

        private void DgvSbInfo_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            System.Drawing.Rectangle Rectangle = new System.Drawing.Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, DgvSbInfo.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), DgvSbInfo.RowHeadersDefaultCellStyle.Font, Rectangle, DgvSbInfo.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private void Frm_Zhcx_Load(object sender, EventArgs e)
        {
            //设置datagridview的列宽自动适应,列不可排序
            for (int i = 0; i < DgvSbInfo.ColumnCount; i++)
            {
                DgvSbInfo.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
            BtnQuery.PerformClick();
        }
        //合同扫描件浏览
        private void DgvSbInfo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (DgvSbInfo.RowCount > 0 && DgvSbInfo.SelectedRows.Count == 1)
            {
                //主键
                Int32 CurHtID = Convert.ToInt32(DgvSbInfo.SelectedRows[0].Cells["htid"].Value);
                //合同编号
                string CurHtbh = DgvSbInfo.SelectedRows[0].Cells["htbh"].Value.ToString();
                //合同名称
                string CurHtmc = DgvSbInfo.SelectedRows[0].Cells["htmc"].Value.ToString();
                //合同签订日期
                string CurHtqdrq = DgvSbInfo.SelectedRows[0].Cells["htqdrq"].Value.ToString().Substring(0, 4);
                string CurScan_flag = DgvSbInfo.SelectedRows[0].Cells["scan_flag"].Value.ToString();
                if (CurScan_flag.Equals("1"))
                {
                    ImageProcesser.Program.CurHtID = CurHtID;
                    ImageProcesser.Program.CurHtbh = CurHtbh;
                    ImageProcesser.Program.CurHtmc = CurHtmc;
                    ImageProcesser.Program.CurHtqdrq = CurHtqdrq;
                    ImageProcesser.ImageProcessorForm ins = new ImageProcesser.ImageProcessorForm();
                    ins.ShowDialog();
                }
            }
        }

        private void DgvSbInfo_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (DgvSbInfo.Columns[e.ColumnIndex].Name.Equals("bf_flag"))
            {
                if (e.Value.ToString().Equals("1"))
                {
                    e.Value = "已报废";
                    //已经借出
                    e.CellStyle.ForeColor = Color.Red;
                }
                else
                {
                    e.Value = "未报废";
                }
            }
            if (DgvSbInfo.Columns[e.ColumnIndex].Name.Equals("jk_flag"))
            {
                if (e.Value.ToString().Equals("1"))
                {
                    e.Value = "进口";
                    //已经扫描
                    e.CellStyle.ForeColor = Color.Green;
                }
                else
                {
                    e.Value = "非进口";
                }
            }
            if (DgvSbInfo.Columns[e.ColumnIndex].Name.Equals("tf_flag"))
            {
                if (e.Value.ToString().Equals("1"))
                {
                    e.Value = "投放";
                    //已经扫描
                    e.CellStyle.ForeColor = Color.Green;
                }
                else
                {
                    e.Value = "非投放";
                }
            }

            if (DgvSbInfo.Columns[e.ColumnIndex].Name.Equals("jc_flag"))
            {
                if (e.Value.ToString().Equals("1"))
                {
                    e.Value = "已借出";
                    //已经借出
                    e.CellStyle.ForeColor = Color.Red;
                }
                else
                {
                    e.Value = "未借出";
                }
            }
            if (DgvSbInfo.Columns[e.ColumnIndex].Name.Equals("scan_flag"))
            {
                if (e.Value.ToString().Equals("1"))
                {
                    e.Value = "已扫描";
                    //已经扫描
                    e.CellStyle.ForeColor = Color.Green;
                }
                else
                {
                    e.Value = "未扫描";
                }
            }


        }
    }
}
