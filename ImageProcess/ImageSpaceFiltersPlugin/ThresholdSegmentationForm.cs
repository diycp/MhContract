﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace ImageSpaceFiltersPlugin
{
    public partial class ThresholdSegmentationForm : Form
    {
        public ThresholdSegmentationForm()
        {
            InitializeComponent();
        }

        public delegate void ThresholdChangedDelegate(int newValue);
        public event ThresholdChangedDelegate ThresholdChanged = null;

        private void thresholdUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (ThresholdChanged != null)
                ThresholdChanged((int)thresholdUpDown.Value);
            thresholdTrackBar.ValueChanged -= new EventHandler(thresholdTrackBar_ValueChanged);
            thresholdTrackBar.Value = (int)thresholdUpDown.Value;
            thresholdTrackBar.ValueChanged += new EventHandler(thresholdTrackBar_ValueChanged);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public int GetThreshold()
        {
            return (int)thresholdUpDown.Value;
        }

        private void thresholdTrackBar_ValueChanged(object sender, EventArgs e)
        {
            thresholdUpDown.Value = thresholdTrackBar.Value;
        }

        private void ThresholdSegmentationForm_Load(object sender, EventArgs e)
        {
            outputComboBox.SelectedIndex = 0;
        }

        public enum OutputType
        {
            ThresholdMask, BelowThreshold, AboveThreshold
        }

        public OutputType GetOutputType()
        {
            switch (outputComboBox.SelectedIndex)
            {
                case 0:
                    return OutputType.ThresholdMask;
                case 1:
                    return OutputType.BelowThreshold;
                default:
                    return OutputType.AboveThreshold;
            }
        }

        private void outputComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            thresholdUpDown_ValueChanged(sender, e);
        }
    }
}
